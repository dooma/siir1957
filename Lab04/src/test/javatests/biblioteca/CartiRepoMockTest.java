package javatests.biblioteca;

import biblioteca.model.Carte;
import biblioteca.repository.repoMock.CartiRepoMock;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class CartiRepoMockTest {

    CartiRepoMock repoMock;

    @Before
    public void setUp() throws Exception {
        repoMock = new CartiRepoMock();
    }

    @Test
    public void adaugaCarteValid() {
        List<Carte> carti = repoMock.getCarti();
        assertEquals(6, carti.size());

        repoMock.adaugaCarteDesfasurat("Poezii", 1970, Arrays.asList("Ion Creanga"), Arrays.asList("povesti"));
        assertEquals(7, carti.size());
    }

    @Test(expected = NumberFormatException.class)
    public void adaugaCarte1() {
        List<Carte> carti = repoMock.getCarti();
        assertEquals(6, carti.size());

        repoMock.adaugaCarteDesfasurat("Poezii", Integer.parseInt("abc"), Arrays.asList("Ion Creanga"), Arrays.asList("povesti"));
        assertEquals(6, carti.size());
    }

    @Test
    public void adaugaCarte2() {
        List<Carte> carti = repoMock.getCarti();
        assertEquals(6, carti.size());

        repoMock.adaugaCarteDesfasurat("Poezii", Integer.MAX_VALUE, Arrays.asList("Ion Creanga"), Arrays.asList("povesti"));
        assertEquals(7, carti.size());
    }

    @Test
    public void adaugaCarte3() {
        List<Carte> carti = repoMock.getCarti();
        assertEquals(6, carti.size());

        repoMock.adaugaCarteDesfasurat("Poezii", Integer.MAX_VALUE - 1, Arrays.asList("Ion Creanga"), Arrays.asList("povesti"));
        assertEquals(7, carti.size());
    }

    @Test
    public void adaugaCarte4() {
        List<Carte> carti = repoMock.getCarti();
        assertEquals(6, carti.size());

        repoMock.adaugaCarteDesfasurat("Poezii", 1961, Arrays.asList("Ion Creanga"), Arrays.asList("povesti"));
        assertEquals(7, carti.size());
    }

    @Test
    public void adaugaCarte5() {
        List<Carte> carti = repoMock.getCarti();
        assertEquals(6, carti.size());

        repoMock.adaugaCarteDesfasurat("Poezii", 0, Arrays.asList("Ion Creanga"), Arrays.asList("povesti"));
        assertEquals(7, carti.size());
    }

    @Test
    public void adaugaCarte6() {
        List<Carte> carti = repoMock.getCarti();
        assertEquals(6, carti.size());

        repoMock.adaugaCarteDesfasurat("Poezii", 1960, new ArrayList<String>(), Arrays.asList("povesti"));
        assertEquals(7, carti.size());
    }

    @Test
    public void adaugaCarte7() {
        List<Carte> carti = repoMock.getCarti();
        assertEquals(6, carti.size());

        repoMock.adaugaCarteDesfasurat("Poezii", 1960, Arrays.asList("Ion Creanga"), new ArrayList<String>());
        assertEquals(7, carti.size());
    }

    @Test
    public void adaugaCarte8() {
        List<Carte> carti = repoMock.getCarti();
        assertEquals(6, carti.size());

        repoMock.adaugaCarteDesfasurat("", 1960, Arrays.asList("Ion Creanga"), new ArrayList<String>());
        assertEquals(7, carti.size());
    }
    

    @Test
    public void cautaCarte1() {
        repoMock.clearCarti();

        List<Carte> carti;
        carti = repoMock.cautaCarte("carte");
        assertEquals(0, carti.size());
    }
    @Test
    public void cautaCarte2() {
        List<Carte> carti;
        carti = repoMock.cautaCarte("abc");
        assertEquals(0, carti.size());
    }
    @Test
    public void cautaCarte3() {
        List<Carte> carti;
        carti = repoMock.cautaCarte("carte");
        assertEquals(0, carti.size());
    }
    @Test
    public void cautaCarte4() {
        List<Carte> carti;
        carti = repoMock.cautaCarte("George");
        assertEquals(1, carti.size());
    }
    @Test
    public void cautaCarte5() {
        List<Carte> carti;
        carti = repoMock.cautaCarte("Mihai");
        assertEquals(2, carti.size());
    }
    @Test
    public void cautaCarte6() {
        List<Carte> carti;
        carti = repoMock.cautaCarte("Sadoveanu");
        assertEquals(1, carti.size());
    }
    @Test
    public void cautaCarte7() {
        List<Carte> carti;
        carti = repoMock.cautaCarte("Calinescu");
        assertEquals(1, carti.size());
    }
    @Test
    public void cautaCarte8() {
        List<Carte> carti;
        carti = repoMock.cautaCarte("asd;kfjas;dlkjfas;dlkjfkdskfjas;ldflksadjflaskjdflaksjd;flakjsdf;lakjsfd;lakjsdf;laksjdf;lkajsd;lfkjasd;lfkjsa;dljfa;lsdkjf;laksjdf;lkasjd;fa;slkdjfa;slkdjf;salkdjf;salkdjf;aslkdjfasd;lfkjsad;lfksjdflksdjfslkdjfasdf");
        assertEquals(0, carti.size());
    }
    @Test
    public void cautaCarte9() {
        List<Carte> carti;
        carti = repoMock.cautaCarte("c");
        assertEquals(4, carti.size());
    }
    @Test
    public void cautaCarte10() {
        List<Carte> carti;
        carti = repoMock.cautaCarte("ca");
        assertEquals(4, carti.size());
    }

    @Test
    public void filtrare1() {
        List<Carte> carti;

        carti = repoMock.getCartiOrdonateDinAnul("1948");
        assertEquals(3, carti.size());
    }

    @Test
    public void filtrare2() {
        List<Carte> carti;

        carti = repoMock.getCartiOrdonateDinAnul("2123");
        assertEquals(0, carti.size());
    }
}
