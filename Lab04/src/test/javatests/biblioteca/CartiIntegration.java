package javatests.biblioteca;

import biblioteca.model.Carte;
import biblioteca.repository.repoMock.CartiRepoMock;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class CartiIntegration {
    CartiRepoMock repoMock;

    @Before
    public void setUp() throws Exception {
        repoMock = new CartiRepoMock();
    }

    @Test
    public void adaugaCarteValid() {
        List<Carte> carti = repoMock.getCarti();
        assertEquals(6, carti.size());

        repoMock.adaugaCarteDesfasurat("Poezii", 1970, Arrays.asList("Ion Creanga"), Arrays.asList("povesti"));
        assertEquals(7, carti.size());
    }

    @Test
    public void cautaCarte1() {
        repoMock.clearCarti();

        List<Carte> carti;
        carti = repoMock.cautaCarte("carte");
        assertEquals(0, carti.size());
    }

    @Test
    public void filtrare1() {
        List<Carte> carti;

        carti = repoMock.getCartiOrdonateDinAnul("1948");
        assertEquals(3, carti.size());
    }


    @Test
    public void integrareBigBang() {
        List<Carte> carti;

        carti = repoMock.cautaCarte("Voiculescu");
        assertEquals(0, carti.size());
        carti = repoMock.getCartiOrdonateDinAnul("2015");
        assertEquals(0, carti.size());
        repoMock.adaugaCarteDesfasurat("Poezii", 2015, Arrays.asList("Ion Voiculescu"), Arrays.asList("povesti"));
        carti = repoMock.cautaCarte("Voiculescu");
        assertEquals(1, carti.size());
        carti = repoMock.getCartiOrdonateDinAnul("2015");
        assertEquals(1, carti.size());
    }

    @Test
    public void integrareTopDown() {
        List<Carte> carti;

        repoMock.adaugaCarteDesfasurat("Poezii", 2015, Arrays.asList("Ion Voiculescu"), Arrays.asList("povesti"));
        carti = repoMock.cautaCarte("Voiculescu");
        assertEquals(1, carti.size());
        carti = repoMock.getCartiOrdonateDinAnul("2015");
        assertEquals(1, carti.size());
    }

}
