package biblioteca.repository.repoMock;


import biblioteca.model.Carte;
import biblioteca.repository.repoInterfaces.CartiRepoInterface;
import biblioteca.util.Validator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class CartiRepoMock implements CartiRepoInterface {

    private List<Carte> carti;

    public CartiRepoMock() {
        carti = new ArrayList<Carte>();

        carti.add(Carte.getCarteFromString("Povesti;Mihai Eminescu,Ion Caragiale,Ion Creanga;1973;Corint;povesti,povestiri"));
        carti.add(Carte.getCarteFromString("Poezii;Sadoveanu;1973;Corint;poezii"));
        carti.add(Carte.getCarteFromString("Enigma Otiliei;George Calinescu;1948;Litera;enigma,otilia"));
        carti.add(Carte.getCarteFromString("Dale carnavalului;Caragiale Ion;1948;Litera;caragiale,carnaval"));
        carti.add(Carte.getCarteFromString("Intampinarea crailor;Mihai Caragiale;1948;Litera;mateiu,crailor"));
        carti.add(Carte.getCarteFromString("Test;;1992;Pipa;am,casa"));

    }

    @Override
    public void adaugaCarte(Carte c) {
        carti.add(c);
    }

    public void adaugaCarteDesfasurat(String title, Integer year, List<String> referenti, List<String> cuvinteCheie) {
        Carte c = new Carte();

        c.setAnAparitie(title);
        for (String s : referenti) {
            c.adaugaReferent(s);
        }

        c.setAnAparitie(year.toString());

        for (String s : cuvinteCheie) {
            c.adaugaCuvantCheie(s);
        }

        carti.add(c);
    }
    @Override
    public List<Carte> cautaCarte(String ref) {
        List<Carte> carti = getCarti(); // 1
        List<Carte> cartiGasite = new ArrayList<Carte>(); // 1
        int i = 0; // 1
        while (i < carti.size()) { // 2
            boolean flag = false; // 3
            List<String> lref = carti.get(i).getReferenti(); // 3
            int j = 0; // 3
            while (j < lref.size()) { // 4
                if (lref.get(j).toLowerCase().contains(ref.toLowerCase())) { // 5
                    flag = true; // 6
                    break; // 7
                }
                j++; // 8
            }
            if (flag == true) { // 9
                cartiGasite.add(carti.get(i)); // 10
            }
            i++; // 11
        }
        return cartiGasite; // 12
    } // 13

    @Override
    public List<Carte> getCarti() {
        return carti;
    }

    @Override
    public void modificaCarte(Carte nou, Carte vechi) {
        // TODO Auto-generated method stub

    }

    @Override
    public void stergeCarte(Carte c) {
        // TODO Auto-generated method stub

    }

    @Override
    public List<Carte> getCartiOrdonateDinAnul(String an) {
        List<Carte> lc = getCarti();
        List<Carte> lca = new ArrayList<Carte>();
        for (Carte c : lc) {
            if (c.getAnAparitie().equals(an)) {
                lca.add(c);
            }
        }

        Collections.sort(lca, new Comparator<Carte>() {

            @Override
            public int compare(Carte a, Carte b) {
                if (a.getTitlu().compareTo(b.getTitlu()) == 0) {
                    return a.getReferenti().get(0).compareTo(b.getReferenti().get(0));
                }

                return a.getTitlu().compareTo(b.getTitlu());
            }

        });

        return lca;
    }

    public void clearCarti() {
        this.carti.clear();
    }

}
